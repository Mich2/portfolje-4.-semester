# Portfølje, 4. semester

Dette er forsiden for portføljen over de 2 emne jeg har valgt at arbejde med på 4. semester på datamatiker uddannelsen.

Jeg har valgt app-udvikling i Xamarin, da det gør muligt bygge videre på C# ved at kunne implementere crossplatform applikationer til Windows, Android og iOS med nye værktøjer. Derudover er IT-sikkerhed og pentesting også valgt.

## README-fil opbygning
Opbygningen for hvert emne vil bestå af en readme fil der vil være struktureret ligeledes:


1. emne/overskrift
2. en beskrivelse/hvad
3. kode eksempel/hvordan
4. resultat/hvorfor
5. litteratur/kilde

Herunder er der en indholdsfortegnelse for Xamarin og IT-sikkerhed pentesting. Denne liste opdateres for hver cyklus.
hver punkt til indholde et overemne og e eller flere underemner.

### Xamarin
- UI
    - Opbyg startside
        - Label
        - inputfelt
        - knapper
        - layout
        - navigationsbar
    - funktionel frontend med backend kode
        - Icommand

### IT-sikkerhed og pentesting
- SQL injection
    - Usikker database
    - Forebyggelse med parametre statements
- Bad authentication and session management
    - Styrke web applikationer med .NET core Identity
        - Password validator
        - Multi factor authentication
        - external authentication
