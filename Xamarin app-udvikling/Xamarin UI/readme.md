# Xamarin startside

Implementeringen af systemet kan påbegyndes, da teamet har lavet skitser i form af wireframes. Disse wireframes illustrerer hvordan teamet forestiller sig hvordan den endelige webapplikationen og mobil applikationen skal se ud. Teamet har taget inspiration fra TER Nordic hjemmeside mht. tema farverne gul, hvid, grå, struktur af kategorier i kasser/knapper, osv. til wireframes. mobil applikationen består af en navigationsbar i bunden i gul med knapper til forsiden og log ind. Øverst fane ses virksomhedens navn evt. logo.  Et søgefelt under navn og logo. Under søgefeltet findes katalog med kategorier for de kemikalier de har i deres udvalg.

## startside

billede her

<br>

## oprettet bruger

Når en kunde har oprettet en brugerkonto på hjemmesiden, bliver log ind og registrer knapperne erstattet med knapperne profil, indkøbskruv og log ud.

billede her

<br>

## underkategorier

Når en af kategorierne bliver valgt bliver der vist en ny side. Den nye siden viser underkategorierne for den valgte kategori. Når en underkategori bliver valgt, vises en ny side med en liste af kemikalierne for underkategorien.

billede her

<br>

## søgning

Når søgefeltet tages i brug, vises der en liste med resultater relevant for søgningen. Ved at klikke på resultatet kan brugeren læses mere om det valgte kemikalie. Derudover kan det valgte kemikalie tilføjes til indkøbskurven med en knap.

billede her

<br>

## indkøbskurven

I indkøbskurven ses de tilføjede kemikalierne og efterspurgte mængde. Med et klik på en knap kan brugeren betale for kemikalierne.

billede her

<br>

Efter et møde med PO, har teamet med eftertanke valgt at bruge engelsk som standard sproget for mobil applikationen, TER Nordic er en international virksomhed. 