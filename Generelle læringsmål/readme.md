# Generelle læringsmål

## Viden - Den studerende har viden om
- Detde valgte emners teori og praksis
- Detde valgte emners relevans i forhold til it‐fagets teori og praksis

## Færdigheder - Den studerende kan
- Udvælge, beskrive og foretage litteratursøgning af en selvvalgt it‐faglig problemstilling
- Diskutere samfundsmæssige aspekter knyttet til detde valgte emner
- Vurdere problemstillinger og opstille løsningsmuligheder i forhold til de valgte emner
- Formidle centrale resultater

## Kompetencer - Den studerende kan
- Selvstændigt sætte sig ind i nye emner inden for fagområdets teori ogeller praksis
- Perspektivere og relatere detde valgte emner i forhold til uddannelsens øvrige emneområder
